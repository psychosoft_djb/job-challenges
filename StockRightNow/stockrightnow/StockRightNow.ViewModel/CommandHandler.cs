﻿// <copyright file="CommandHandler.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.ViewModel
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Handler for command buttons 
    /// </summary>
    public class CommandHandler : ICommand
    {
        /// <summary>
        /// The action to perform 
        /// </summary>
        private Action action;

        /// <summary>
        /// Can the action be executed 
        /// </summary>
        private bool canExecute;

        /// <summary>
        /// Initalise a new instance of the CommandHandler 
        /// </summary>
        /// <param name="action">     The action to perform </param>
        /// <param name="canExecute"> Can the action be executed </param>
        public CommandHandler(Action action, bool canExecute)
        {
            this.action = action;
            this.canExecute = canExecute;
        }

        /// <summary>
        /// Event handler if the can execute changes 
        /// </summary>
        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Can the command be executed for the given object 
        /// </summary>
        /// <param name="parameter"> the parameter to check </param>
        /// <returns> true if the command can be executed, false if not </returns>
        public bool CanExecute(object parameter)
        {
            return this.canExecute;
        }

        /// <summary>
        /// Execute the action for the given object 
        /// </summary>
        /// <param name="parameter"> The object to check </param>
        public void Execute(object parameter)
        {
            this.action();
        }
    }
}