﻿// <copyright file="DatabaseConnection.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model.DAL
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Data.SqlClient;

    internal class DatabaseConnection
    {
        /// <summary>
        /// Get the connection string for the database. 
        /// </summary>
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["StockRightNow"].ConnectionString; }
        }

        /// <summary>
        /// Make a new parameter for a command. 
        /// </summary>
        /// <param name="command">                        
        /// The command to make the parameter for.
        /// </param>
        /// <param name="name">                            The name of the parameter. </param>
        /// <param name="parameterType">                   The parameter type. </param>
        /// <param name="value">                           The value of the parameter. </param>
        /// <param name="maxStringSize">                   The maximum size a string valye can be. </param>
        /// <param name="replaceNullValueWithEmptyString">
        /// Should a null value be replaced with an empty string. Defaults to true.
        /// </param>
        /// <returns> A new populated parameter or null. </returns>
        public static IDataParameter MakeNewCommandParameter(IDbCommand command, string name, DbType parameterType, object value, int maxStringSize, bool replaceNullValueWithEmptyString = true)
        {
            IDataParameter result = null;

            if (command != null)
            {
                result = command.CreateParameter();
                result.ParameterName = name;
                result.DbType = parameterType;
                result.Value = GetValidatedObject(value, maxStringSize, replaceNullValueWithEmptyString);
            }

            return result;
        }

        /// <summary>
        /// Make a new parameter for a command. 
        /// </summary>
        /// <param name="command">                        
        /// The command to make the parameter for.
        /// </param>
        /// <param name="name">                            The name of the parameter. </param>
        /// <param name="parameterType">                   The parameter type. </param>
        /// <param name="value">                           The value of the parameter. </param>
        /// <param name="replaceNullValueWithEmptyString">
        /// Should a null value be replaced with an empty string. Defaults to true.
        /// </param>
        /// <returns> A new populated parameter or null. </returns>
        public static IDataParameter MakeNewCommandParameter(IDbCommand command, string name, DbType parameterType, object value, bool replaceNullValueWithEmptyString = true)
        {
            return MakeNewCommandParameter(command, name, parameterType, value, 0, replaceNullValueWithEmptyString);
        }

        /// <summary>
        /// Make a new open database connection 
        /// </summary>
        /// <returns> A new open database connection </returns>
        public static IDbConnection MakeOpenDatabaseConnection()
        {
            var result = new SqlConnection(ConnectionString);
            result.Open();

            return result;
        }

        /// <summary>
        /// Get a valid object to be used for the database. 
        /// </summary>
        /// <param name="value">                          
        /// The value to get a valid value for.
        /// </param>
        /// <param name="maxStringSize">                  
        /// The maximum size a string value could be.
        /// </param>
        /// <param name="replaceNullValueWithEmptyString"> Should null values be replaced with an empty string. </param>
        /// <returns> The valid object to use. </returns>
        private static object GetValidatedObject(object value, int maxStringSize, bool replaceNullValueWithEmptyString)
        {
            object result = value;

            if (value != null)
            {
                var valueType = value.GetType();

                if (valueType == typeof(string))
                {
                    result = TruncateString((string)value, maxStringSize);
                }
                else if (valueType == typeof(DateTime))
                {
                    result = GetValidDate((DateTime)value);
                }
            }
            else if (replaceNullValueWithEmptyString)
            {
                result = string.Empty;
            }

            return result;
        }

        /// <summary>
        /// Get a valid date time value. 
        /// </summary>
        /// <param name="value"> The value to base the result on. </param>
        /// <returns> The value with the year set to at least 1800 or null. </returns>
        private static DateTime GetValidDate(DateTime value)
        {
            var result = value;
            if (value != null)
            {
                if (value.Year < 1800)
                {
                    result = new DateTime(1800, value.Month, value.Day, value.Hour, value.Minute, value.Second);
                }
            }

            return result;
        }

        /// <summary>
        /// Truncate a string to a given length. 
        /// </summary>
        /// <param name="value"> The string to truncate. </param>
        /// <param name="size">  The maximum length of the string </param>
        /// <returns> The string truncated to the given length or an empty string. </returns>
        private static string TruncateString(string value, int size)
        {
            var result = value ?? string.Empty;
            var maxSize = size > 0 ? size : result.Length;
            return result.Length > maxSize ? result.Substring(0, maxSize) : result;
        }
    }
}