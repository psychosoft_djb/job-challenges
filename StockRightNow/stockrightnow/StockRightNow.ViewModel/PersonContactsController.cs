﻿// <copyright file="PersonContactsController.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.ViewModel
{    
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    using StockRightNow.Model;
    using StockRightNow.Model.DAL;

    public class PersonContactsController : INotifyPropertyChanged
    {
        /// <summary>
        /// The contacts being controlled 
        /// </summary>
        private ObservableCollection<ContactController> contacts;

        /// <summary>
        /// the personDal to use 
        /// </summary>
        private IContactDal dal;

        /// <summary>
        /// the person the contacts are for 
        /// </summary>
        private IPerson person;

        /// <summary>
        /// Initalise a new instance of PersonContactsController 
        /// </summary>
        /// <param name="dal">    The personDal to talk to the persistance on </param>
        /// <param name="person"> the person the contacts are about </param>
        public PersonContactsController(IContactDal dal, IPerson person)
        {
            this.dal = dal;
            this.person = person;
            this.GetContacts();
        }

        /// <summary>
        /// Get the contacts 
        /// </summary>
        public ObservableCollection<ContactController> Contacts
        {
            get
            {
                return this.contacts;
            }
        }

        /// <summary>
        /// Envent handler for the property change 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Save all unsaved contacts 
        /// </summary>
        public void Save()
        {
            foreach (var contact in this.contacts)
            {
                contact.Save();
            }

            this.GetContacts();
        }

        /// <summary>
        /// Add a new contact 
        /// </summary>
        public void Add()
        {
            if (this.contacts == null)
            {
                this.contacts = new ObservableCollection<ContactController>();
            }

            var toAdd = new ContactController(this.dal, null);
            toAdd.PersonId = this.person.ID;
            toAdd.ContactDate = System.DateTime.Now;
            this.contacts.Add(toAdd);

            NotifyPropertyChanged("Contacts");
        }

        /// <summary>
        /// Update the person who is associated with the contacts 
        /// </summary>
        /// <param name="person"> the person who is associated </param>
        public void UpdatePerson(IPerson person)
        {
            if (this.person.ID != person.ID)
            {
                this.person = person;

                for (var i = 0; i < this.contacts.Count; i++)
                {
                    this.contacts[i].PersonId = this.person.ID;
                    this.contacts[i].Id = 0;
                }

                NotifyPropertyChanged("Contacts");
            }
        }

        /// <summary>
        /// Get the contacts for the person 
        /// </summary>
        private void GetContacts()
        {
            this.contacts = new ObservableCollection<ContactController>();
            var data = this.dal.GetContactHistory(this.person);

            foreach (var contact in data)
            {
                contacts.Add(new ContactController(this.dal, contact));
            }

            NotifyPropertyChanged("Contacts");
        }

        /// <summary>
        /// Property change handler 
        /// </summary>
        /// <param name="propertyName"> The name of the property thats changed </param>
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}