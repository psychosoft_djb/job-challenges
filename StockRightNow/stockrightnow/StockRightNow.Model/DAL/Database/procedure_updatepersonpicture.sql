USE [StockRightNow]
GO

/****** Object:  StoredProcedure [dbo].[UpdatePersonPicture]    Script Date: 08/03/2017 01:18:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdatePersonPicture](@ID int, @Picture nvarchar(max))
AS
BEGIN
	update dbo.person set image = @picture where id = @ID			
END

GO

