﻿// <copyright file="CsvFeedInterpreterTest.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Data.Tests
{
    using System.IO;
    using System.Text;
    using System.Linq;
    using NUnit.Framework;
    using System;

    /// <summary>
    /// Csv feed interpreter test.
    /// </summary>
    [TestFixture]
    public class CsvFeedInterpreterTest
    {
        /// <summary>
        /// The csv header.
        /// </summary>
        const string CsvHeader = "ORDERID,TRANSACTIONDATE,ITEMCODE,DESCRIPTION,PRICE,QUANTITY";

        /// <summary>
        /// A basic order item.
        /// </summary>
        private readonly OrderItem BasicOrderItem = new OrderItem
        {
            OrderId = 2469,
            TransactionDate = new DateTime(2016, 05, 29),
            ItemCode = "MS55999",
            Description = "Zig Memory System Wink of Stella Brush Glitter Marker Clear",
            Price = (decimal)4.99,
            Quantity = 1
        };

        /// <summary>
        /// Test the interpreter Implements the IFeedInterpreter interface.
        /// </summary>
        [Test]
        public void ImplementsIFeedInterpreterInterface()
        {
            var test = new CsvFeedInterpreter();
            Assert.IsInstanceOf<IFeedInterpreter>(test);
        }

        /// <summary>
        /// Test that a header the produces no orders.
        /// </summary>
        [Test]
        public void HeaderProducesNoOrders()
        {
            var test = new CsvFeedInterpreter();
            var result = test.ReadOrdersFromFeed(StringToStream(CsvHeader));
            Assert.IsTrue(result.Count == 0, string.Format("Expected 0 results got {0}", result.Count));
        }

        /// <summary>
        /// Test that a header and an order line produces one orders.
        /// </summary>
        [Test]
        public void HeaderAndOneLineProducesOneOrder()
        {
            var test = new CsvFeedInterpreter();
            var result = test.ReadOrdersFromFeed(StringToStream(CsvHeader + "\n2469,29/05/2016,MS55999,Zig Memory System Wink of Stella Brush Glitter Marker Clear,4.99,1"));
            Assert.IsTrue(result.Count == 1, string.Format("Expected 1 results got {0}", result.Count));
        }

        /// <summary>
        /// Test a null stream produces no orders.
        /// </summary>
        [Test]
        public void NullStreamProducesNoOrders()
        {
            var test = new CsvFeedInterpreter();
            var result = test.ReadOrdersFromFeed(null);
            Assert.IsTrue(result.Count == 0, string.Format("Expected 0 results got {0}", result.Count));
        }

        /// <summary>
        /// Test that an ended stream produces no orders.
        /// </summary>
        [Test]
        public void EndedStreamProducesNoOrders()
        {
            var test = new CsvFeedInterpreter();
            var stream = StringToStream("\n");
            stream.ReadLine();
            var result = test.ReadOrdersFromFeed(stream);
            Assert.IsTrue(result.Count == 0, string.Format("Expected 0 results got {0}", result.Count));
        }

        /// <summary>
        /// Test than an order is added correctly.
        /// </summary>
        [Test]
        public void OrderAddedCorrectly()
        {
            var test = new CsvFeedInterpreter();
            var result = test.ReadOrdersFromFeed(StringToStream(OrderItemToCsv(this.BasicOrderItem)));

            var order = result.First();
            this.OrderItemsAreEqual(this.BasicOrderItem, order);
        }

        /// <summary>
        /// Test that adding the same order identifier twice adds one order with two items.
        /// </summary>
        [Test]
        public void SameOrderIdTwiceAddsOneOrderWithTwoItems()
        {
            var secondItem = new OrderItem
            {
                OrderId = this.BasicOrderItem.OrderId,
                Description = this.BasicOrderItem.Description,
                Price = this.BasicOrderItem.Price,
                Quantity = this.BasicOrderItem.Quantity,
                TransactionDate = this.BasicOrderItem.TransactionDate,
                ItemCode = "bob"
            };
            var data = string.Format("{0}\n{1}\n{2}\n", CsvHeader, OrderItemToCsv(this.BasicOrderItem), OrderItemToCsv(secondItem));
            var test = new CsvFeedInterpreter();
            var result = test.ReadOrdersFromFeed(StringToStream(data));

            Assert.IsTrue(result.Count == 2, string.Format("Expected 2 items in the order and got {0}", result.Count));
        }

        /// <summary>
        /// Are the order items are equal.
        /// </summary>
        /// <param name="left">Left test.</param>
        /// <param name="right">Right test.</param>
        private void OrderItemsAreEqual(OrderItem left, OrderItem right)
        {
            Assert.IsTrue(left.OrderId == right.OrderId, "The order ID's are different");
            Assert.IsTrue(string.Compare(left.Description, right.Description, StringComparison.Ordinal) == 0, "Decriptions are different");
            Assert.IsTrue(string.Compare(left.ItemCode, right.ItemCode, StringComparison.Ordinal) == 0, "Item codes are different");
            Assert.IsTrue(left.Price == right.Price, "Prices are different");
            Assert.IsTrue(left.Quantity == right.Quantity, "Quantities are different");
            Assert.IsTrue(DateTime.Compare(left.TransactionDate, right.TransactionDate) == 0, "Transaction Dates are different");
        }

        /// <summary>
        /// Change an order item to csv.
        /// </summary>
        /// <returns>The item as csv.</returns>
        /// <param name="item">The item.</param>
        private string OrderItemToCsv(OrderItem item)
        {
            return string.Format("{0},{1},{2},{3},{4},{5}", item.OrderId, item.TransactionDate, item.ItemCode, item.Description, item.Price, item.Quantity);
        }

        /// <summary>
        /// Convert a string to a stream.
        /// </summary>
        /// <returns>The stream.</returns>
        /// <param name="value">Value to convert.</param>
        private StreamReader StringToStream(string value)
        {
            var bytes = Encoding.UTF8.GetBytes(value);
            var memStream = new MemoryStream(bytes);
            return new StreamReader(memStream);
        }

    }
}
