USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PersonContactHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Action] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
 CONSTRAINT [PK_PersonContactHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[PersonContactHistory]  WITH CHECK ADD  CONSTRAINT [FK_PersonContactHistory_Person] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PersonContactHistory] CHECK CONSTRAINT [FK_PersonContactHistory_Person]
GO

