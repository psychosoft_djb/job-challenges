USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPersonIDs]
AS
BEGIN
	SET NOCOUNT ON;
	select ID from dbo.person
END

GO

