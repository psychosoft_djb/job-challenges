USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AddPerson](@Forename nvarchar(50), @surname nvarchar(50), @dob date, @Picture nvarchar(max))
AS
BEGIN
	insert into dbo.person (forename, surname, dateofbirth, image) values(@forename, @surname, @dob, @picture)
	select max(ID) as ID from dbo.person
END

GO

