USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPerson](@ID int)	
AS
BEGIN
	SET NOCOUNT ON;
	select ID,Forename,Surname,DateOfBirth,Image from dbo.person where id = @ID
END

GO

