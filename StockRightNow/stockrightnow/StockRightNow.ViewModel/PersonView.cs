﻿// <copyright file="PersonView.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.ViewModel
{
    using System;
    using System.ComponentModel;
    using System.Windows.Forms;
    using System.Windows.Input;

    using StockRightNow.Model;
    using StockRightNow.Model.DAL;

    /// <summary>
    /// View moddle for the person view 
    /// </summary>
    public class PersonView : INotifyPropertyChanged
    {
        /// <summary>
        /// The DAL to use for the person 
        /// </summary>
        private readonly IPersonDal personDal;

        /// <summary>
        /// The DAL to use for the contacts 
        /// </summary>
        private readonly IContactDal contactDal;

        /// <summary>
        /// The controller holding the person IDs 
        /// </summary>
        private readonly PersonIdsController idController;

        /// <summary>
        /// The contacts for the current person 
        /// </summary>
        private PersonContactsController contacts;

        /// <summary>
        /// The command to add a person 
        /// </summary>
        private ICommand commandAddPerson;

        /// <summary>
        /// The command to change the persons picture 
        /// </summary>
        private ICommand commandChangePictureCommand;

        /// <summary>
        /// The command to delete the current person 
        /// </summary>
        private ICommand commandDeleteCurrentPerson;

        /// <summary>
        /// The command to go to the next person 
        /// </summary>
        private ICommand commandNextPerson;

        /// <summary>
        /// The command to add a new contact 
        /// </summary>
        private ICommand commandAddContact;

        /// <summary>
        /// The command to go to the previous person 
        /// </summary>
        private ICommand commandPreviousPerson;

        /// <summary>
        /// The command to save the current person 
        /// </summary>
        private ICommand commandSaveCurrentPerson;

        /// <summary>
        /// The controller for the current person 
        /// </summary>
        private Model.PersonController currentPerson;

        /// <summary>
        /// Initalise a new instance of the PersonView 
        /// </summary>
        public PersonView()
        {
            this.personDal = new PersonDal();
            this.contactDal = new ContactDal();
            this.idController = new PersonIdsController(this.personDal);
            this.ChangePerson(this.idController.FirstId);
        }

        /// <summary>
        /// Evenet handler for the change of a property 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the command to add a person 
        /// </summary>
        public ICommand CommandAddPerson
        {
            get
            {
                return this.commandAddPerson ?? (this.commandAddPerson = new CommandHandler(() => this.AddPerson(), true));
            }
        }

        /// <summary>
        /// Gets the command to add a contact 
        /// </summary>
        public ICommand CommandAddContact
        {
            get
            {
                return this.commandAddContact ?? (this.commandAddContact = new CommandHandler(() => this.AddContact(), true));
            }
        }

        /// <summary>
        /// Gets the command to change the picture 
        /// </summary>
        public ICommand CommandChangePicture
        {
            get
            {
                return this.commandChangePictureCommand ?? (this.commandChangePictureCommand = new CommandHandler(() => this.ChangePicture(), true));
            }
        }

        /// <summary>
        /// Gets the command to delete the current person 
        /// </summary>
        public ICommand CommandDeletePerson
        {
            get
            {
                return this.commandDeleteCurrentPerson ?? (this.commandDeleteCurrentPerson = new CommandHandler(() => this.DeletePerson(), true));
            }
        }

        /// <summary>
        /// Gets the command to goto the next person 
        /// </summary>
        public ICommand CommandNextPerson
        {
            get
            {
                return this.commandNextPerson ?? (this.commandNextPerson = new CommandHandler(() => this.NextPerson(), true));
            }
        }

        /// <summary>
        /// Gets the command to goto the previous person 
        /// </summary>
        public ICommand CommandPreviousPerson
        {
            get
            {
                return this.commandPreviousPerson ?? (this.commandPreviousPerson = new CommandHandler(() => this.PreviousPerson(), true));
            }
        }

        /// <summary>
        /// Gets the command to save the current person 
        /// </summary>
        public ICommand CommandSavePerson
        {
            get
            {
                return this.commandSaveCurrentPerson ?? (this.commandSaveCurrentPerson = new CommandHandler(() => this.SavePerson(), true));
            }
        }

        /// <summary>
        /// Gets and sets the current persons date of birth 
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return this.currentPerson.DateOfBirth;
            }

            set
            {
                this.currentPerson.DateOfBirth = value;
                RaisePropertyChanged("DateOfBirth");
            }
        }

        /// <summary>
        /// Gets and set the current persons firstname 
        /// </summary>
        public string Firstname
        {
            get
            {
                return this.currentPerson.Firstname;
            }

            set
            {
                this.currentPerson.Firstname = value;
                RaisePropertyChanged("Firstname");
            }
        }

        /// <summary>
        /// Gets and set the current persons lastname 
        /// </summary>
        public string Lastname
        {
            get
            {
                return this.currentPerson.Lastname;
            }

            set
            {
                this.currentPerson.Lastname = value;
                RaisePropertyChanged("Lastname");
            }
        }

        /// <summary>
        /// Get the contacts for the current person 
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<ContactController> Contacts
        {
            get
            {
                return this.contacts.Contacts;
            }
        }

        /// <summary>
        /// Gets the path to the currents persons picture if it exists or an empty string 
        /// </summary>
        public string Picture
        {
            get
            {
                return System.IO.File.Exists(this.currentPerson.Photograph) ? this.currentPerson.Photograph : string.Empty;
            }
        }

        /// <summary>
        /// Add a new person 
        /// </summary>
        public void AddPerson()
        {
            this.currentPerson = new Model.PersonController(this.personDal, -1);
            this.idController.Add(this.currentPerson.ID);
            this.AllChanged();
        }

        /// <summary>
        /// Change the picture of the current person 
        /// </summary>
        public void ChangePicture()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Title = "Selct Photograph";
                dialog.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.png";

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    this.currentPerson.Photograph = dialog.FileName;
                    RaisePropertyChanged("Picture");
                }
            }
        }

        /// <summary>
        /// Delete the current person 
        /// </summary>
        public void DeletePerson()
        {
            this.idController.IdList.Remove(this.currentPerson.ID);
            this.currentPerson.Delete();
            //this.currentPerson = new PersonController(this.personDal, this.idController.GetPreviousId(this.currentPerson.ID));
            this.PreviousPerson();
        }

        /// <summary>
        /// Goto the next person 
        /// </summary>
        public void NextPerson()
        {
            this.ChangePerson(this.idController.GetNextId(this.currentPerson.ID));
        }

        /// <summary>
        /// Goto the previous person 
        /// </summary>
        public void PreviousPerson()
        {
            this.ChangePerson(this.idController.GetPreviousId(this.currentPerson.ID));
        }

        /// <summary>
        /// Add a contact to the contact history 
        /// </summary>
        public void AddContact()
        {
            this.contacts.Add();
        }

        /// <summary>
        /// Save the current person 
        /// </summary>
        public void SavePerson()
        {
            var currentId = this.currentPerson.ID;
            this.currentPerson.Save();
            this.idController.ChangeIdValue(currentId, this.currentPerson.ID);
            this.contacts.UpdatePerson(this.currentPerson);
            this.contacts.Save();

            this.AllChanged();
        }

        /// <summary>
        /// Raise changes on all properties 
        /// </summary>
        private void AllChanged()
        {
            RaisePropertyChanged("Firstname");
            RaisePropertyChanged("Lastname");
            RaisePropertyChanged("DateOfBirth");
            RaisePropertyChanged("Picture");
            RaisePropertyChanged("Contacts");
        }

        /// <summary>
        /// Change the current person to the one with the given ID 
        /// </summary>
        /// <param name="toId"> The ID of the person to change to </param>
        private void ChangePerson(long toId)
        {
            if (toId > 0)
            {
                this.currentPerson = new Model.PersonController(this.personDal, toId);
                this.contacts = new PersonContactsController(this.contactDal, this.currentPerson);
                this.AllChanged();
            }
        }

        /// <summary>
        /// Raise a new propery change event 
        /// </summary>
        /// <param name="propertyName"> The name of the changed property </param>
        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}