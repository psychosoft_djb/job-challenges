USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeletePerson](@ID int)
AS
BEGIN
	delete from dbo.person where id = @ID
END

GO

