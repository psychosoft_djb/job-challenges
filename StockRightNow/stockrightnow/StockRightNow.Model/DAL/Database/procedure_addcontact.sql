USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[AddContact](@PersonID int, @action nvarchar(max), @notes nvarchar(max))
AS
BEGIN
	if exists(select id from dbo.person where id = @personid)	
	begin
		insert into dbo.PersonContactHistory (personid, [date], [action], notes) values(@personid, getdate(), @action, @notes)
		select max(ID) as ID from dbo.PersonContactHistory
	end
END


GO

