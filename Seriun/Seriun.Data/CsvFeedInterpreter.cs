﻿// <copyright file="CsvFeedInterpreter.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Csv feed interpreter.
    /// </summary>
    public class CsvFeedInterpreter : IFeedInterpreter
    {
        // TODO : change the hard coded feild details to be looked up from the header record

        /// <summary>
        /// The number of fields in an order item.
        /// </summary>
        const int OrderItemFieldCount = 6;

        /// <summary>
        /// The order identifier field number.
        /// </summary>
        const int OrderIdField = 0;

        /// <summary>
        /// The transaction date field number.
        /// </summary>
        const int TransactionDateField = 1;

        /// <summary>
        /// The item code field number.
        /// </summary>
        const int ItemCodeField = 2;

        /// <summary>
        /// The description field number.
        /// </summary>
        const int DescriptionField = 3;

        /// <summary>
        /// The price field number.
        /// </summary>
        const int PriceField = 4;

        /// <summary>
        /// The quantity field number.
        /// </summary>
        const int QuantityField = 5;

        /// <summary>
        /// Reads the orders from feed.
        /// </summary>
        /// <returns>The orders from feed.</returns>
        /// <param name="feed">Feed.</param>
        public List<OrderItem> ReadOrdersFromFeed(StreamReader feed)
        {
            var result = new List<OrderItem>();
            var line = string.Empty;

            if (feed != null)
            {
                while ((line = feed.ReadLine()) != null)
                {
                    var values = this.GetValues(line);
                    if (IsDataList(values))
                    {
                        this.AddOrderItem(values, result);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Adds the order item to the orders.
        /// </summary>
        /// <param name="values">The values of the order item.</param>
        /// <param name="orders">The orders to add to.</param>
        private void AddOrderItem(List<string> values, List<OrderItem> orders)
        {
            var item = MakeOrderItemFromValues(values);
            if (item != null)
            {
                if (orders == null)
                {
                    orders = new List<OrderItem>();
                }

                orders.Add(item);
            }
        }

        /// <summary>
        /// Makes the order item from values.
        /// </summary>
        /// <returns>The order item from values or null.</returns>
        /// <param name="values">Values to make the order item from.</param>
        private OrderItem MakeOrderItemFromValues(List<string> values)
        {
            OrderItem item = null;

            if (values != null)
            {
                if (values.Count >= OrderItemFieldCount)
                {
                    long id;
                    DateTime transactionDate;
                    long quantity;
                    decimal price;

                    long.TryParse(values[OrderIdField], out id);
                    DateTime.TryParse(values[TransactionDateField], out transactionDate);
                    decimal.TryParse(values[PriceField], out price);
                    long.TryParse(values[QuantityField], out quantity);

                    item = new OrderItem
                    {
                        OrderId = id,
                        TransactionDate = transactionDate,
                        Quantity = quantity,
                        Price = price,
                        ItemCode = values[ItemCodeField],
                        Description = values[DescriptionField]
                    };
                }
            }

            return item;
        }

        /// <summary>
        /// Gets the values from a string.
        /// </summary>
        /// <returns>The values seperated from the string.</returns>
        /// <param name="value">the value to get values from.</param>
        private List<string> GetValues(string value)
        {
            List<string> values = null;

            if (!string.IsNullOrEmpty(value))
            {
                values = value.Split(',').ToList();
            }

            return values ?? new List<string>();
        }

        /// <summary>
        /// Is the given list a data list or not.
        /// </summary>
        /// <returns><c>true</c>, if data is in the list, <c>false</c> otherwise.</returns>
        /// <param name="values">The values to test.</param>
        private bool IsDataList(List<string> values)
        {
            var result = false;

            if (values != null)
            {
                if (values.Count > 0)
                {
                    var first = 0;
                    result = int.TryParse(values.First(), out first);
                }

            }

            return result;
        }
    }
}
