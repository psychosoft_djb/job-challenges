﻿// <copyright file="AverageOrderValueReport.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Reports
{
    using System.Collections.Generic;
    using System.Linq;
    using Seriun.Data;

    /// <summary>
    /// Average order value report.
    /// </summary>
    public class AverageOrderValueReport : IReport
    {
        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns>The average order value.</returns>
        /// <param name="orders">The orders to check.</param>
        public string GetResult(List<OrderItem> orders)
        {
            var result = string.Empty;

            if (orders != null)
            {

                var averageAll = from item in orders
                                 group item by new { Group = item.OrderId } into grouped
                                 select new
                                 {
                                     averaged = grouped.Average(x => (x.Price * x.Quantity))
                                 };

                var average = averageAll.Sum(x => x.averaged) / averageAll.Count();

                result = string.Format("The average value of each order is {0} over {1} orders", average.ToString("C"), averageAll.Count());
            }

            return result;
        }

        /// <summary>
        /// The parameterses to use when running the report next.
        /// </summary>
        /// <param name="parameters">List of parameters.</param>
        public void ParametersToUse(List<string> parameters)
        {
            // no parameters needed
        }
    }
}
