﻿// <copyright file="FileLoaderTest.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Data.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// File loader test.
    /// </summary>
    [TestFixture]
    public class FileLoaderTest
    {
        /// <summary>
        /// The test csv file.
        /// This will only work on my machine at home.
        /// </summary>
        private static string TestCsvFile = "/Users/david/development/mono/Seriun/feed.csv";

        /// <summary>
        /// Test bad file names are handled.
        /// </summary>
        /// <param name="filename">The filename to test.</param>
        [TestCase("")]
        [TestCase("   ")]
        [TestCase(null)]
        [TestCase("../../bob")]
        public void BadFileNameHandled(string filename)
        {
            var result = FileLoader.GetStream(filename);
            Assert.IsNull(result, "didn't get a null stream for a bad name");
        }

        /// <summary>
        /// Tests the test file is found.
        /// </summary>
        [Test]
        public void TestFileIsFound()
        {
            var result = FileLoader.GetStream(TestCsvFile);
            Assert.IsNotNull(result, "couldn't open the test csv");
        }
    }
}
