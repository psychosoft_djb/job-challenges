﻿// <copyright file="IPersonDal.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model.DAL
{
    using System.Collections.Generic;

    /// <summary>
    /// Dal definition for persisting person data 
    /// </summary>
    public interface IPersonDal
    {
        /// <summary>
        /// Adds a person to the database. 
        /// </summary>
        /// <param name="person"> The person to add. </param>
        /// <returns> The ID of the person added or -1. </returns>
        long AddPerson(IPerson person);

        /// <summary>
        /// Delete a given person based on ID. 
        /// </summary>
        /// <param name="toDelete"> the person to delete. </param>
        /// <returns> true if the delete worked, false if not. </returns>
        bool DeletePerson(IPerson toDelete);

        /// <summary>
        /// Gets a person for a given ID 
        /// </summary>
        /// <param name="personId"> The ID to get. </param>
        /// <returns> The loaded person or null. </returns>
        IPerson GetPerson(long personId);

        /// <summary>
        /// Get the list of all the person ID's availabe. 
        /// </summary>
        /// <returns> List of all the person IDs. </returns>
        List<long> GetPersonIds();
    }
}