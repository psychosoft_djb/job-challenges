﻿// <copyright file="PersonIdsController.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.ViewModel
{    
    using System.Collections.Generic;

    using StockRightNow.Model.DAL;

    /// <summary>
    /// Controller for working with person data 
    /// </summary>
    public class PersonIdsController
    {
        /// <summary>
        /// The personDal to use when accessing the database for person data 
        /// </summary>
        private readonly IPersonDal dal;

        /// <summary>
        /// The list of person ids 
        /// </summary>
        private List<long> personIds;

        /// <summary>
        /// Initialise a new instance of PersonIdController 
        /// </summary>
        /// <param name="dal"> The personDal to use when getting person data </param>
        public PersonIdsController(IPersonDal dal)
        {
            this.dal = dal;
            this.Refresh();
        }

        /// <summary>
        /// Get the list of IDs for person data that is available 
        /// </summary>
        public List<long> IdList
        {
            get
            {
                return this.personIds;
            }
        }

        /// <summary>
        /// Get the last ID value from the IdList 
        /// </summary>
        public long LastId
        {
            get
            {
                return this.personIds.Count > 0 ? this.personIds[this.personIds.Count - 1] : -1;
            }
        }

        /// <summary>
        /// Get the first ID value from the IdList 
        /// </summary>
        public long FirstId
        {
            get
            {
                return this.personIds.Count > 0 ? this.personIds[0] : -1;
            }
        }

        /// <summary>
        /// Get the ID of the value after the given value in the IdList 
        /// </summary>
        /// <param name="idBefore"> The value before the one being returned </param>
        /// <returns> The value after the given value in the IdList or the last id. </returns>
        public long GetNextId(long idBefore)
        {
            long id = -1;

            for (var i = 0; i < this.personIds.Count; i++)
            {
                if (this.personIds[i] == idBefore)
                {
                    var increment = i + 1 < this.personIds.Count ? 1 : 0;
                    id = this.personIds[i + increment];
                    break;
                }
            }

            return id <= 0 ? this.LastId : id;
        }

        /// <summary>
        /// Get the ID of the value before the given value in the IdList 
        /// </summary>
        /// <param name="idAfter"> The value after the one being returned </param>
        /// <returns> The value before the given value in the IdList or the first id. </returns>
        public long GetPreviousId(long idAfter)
        {
            long id = -1;

            for (var i = this.personIds.Count - 1; i > 0; i--)
            {
                if (this.personIds[i] == idAfter)
                {
                    var decrement = i - 1 >= 0 ? 1 : 0;
                    id = this.personIds[i - decrement];
                    break;
                }
            }

            return id <= 0 ? this.FirstId : id;
        }

        /// <summary>
        /// Change the value of an ID to something else. 
        /// </summary>
        /// <param name="oldId"> The ID to change </param>
        /// <param name="newId"> The new value of the ID </param>
        public void ChangeIdValue(long oldId, long newId)
        {
            if (this.personIds.Contains(oldId))
            {
                for (var i = 0; i < this.personIds.Count; i++)
                {
                    if (this.personIds[i] == oldId)
                    {
                        this.personIds[i] = newId;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Add an id to the list 
        /// </summary>
        /// <param name="id"> the id to add </param>
        public void Add(long id)
        {
            if (this.personIds == null)
            {
                this.personIds = new List<long>();
            }

            this.personIds.Add(id);
        }

        /// <summary>
        /// Reload all the IDs 
        /// </summary>
        public void Refresh()
        {
            this.personIds = this.dal.GetPersonIds();
        }
    }
}