﻿// <copyright file="PersonController.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model
{
    using System;

    using StockRightNow.Model.DAL;    

    /// <summary>
    /// Controller to provide high level access to person data 
    /// </summary>
    public class PersonController : IPerson
    {
        /// <summary>
        /// The DAL to use when persisting this data 
        /// </summary>
        private readonly IPersonDal personDal;

        /// <summary>
        /// has the data been changed 
        /// </summary>
        private bool dirty;

        /// <summary>
        /// The person being controlled 
        /// </summary>
        private IPerson person;

        /// <summary>
        /// Initalise a new instance of the PersonController 
        /// </summary>
        /// <param name="personDal"> The DAL to use when communicationg with the persistance for a person </param>
        /// <param name="id">       
        /// The ID of the person to load. A new person will be created if the ID does not already exist.
        /// </param>
        public PersonController(IPersonDal personDal, long id)
        {
            this.personDal = personDal;

            person = this.personDal.GetPerson(id) ?? new Person();
            dirty = false;
        }

        /// <summary>
        /// Get and set the date of birth of the person 
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return this.person.DateOfBirth;
            }

            set
            {
                this.person.DateOfBirth = value;
                dirty = true;
            }
        }

        /// <summary>
        /// Get and set the forename of the person 
        /// </summary>
        public string Firstname
        {
            get
            {
                return this.person.Firstname;
            }

            set
            {
                this.person.Firstname = value ?? string.Empty;
                dirty = true;
            }
        }

        /// <summary>
        /// Get the ID of the person 
        /// </summary>
        public long ID
        {
            get
            {
                return this.person.ID;
            }

            set
            {
                // not allowed to do this here
            }
        }

        /// <summary>
        /// Get and set the surname of the person 
        /// </summary>
        public string Lastname
        {
            get
            {
                return this.person.Lastname;
            }

            set
            {
                this.person.Lastname = value ?? string.Empty;
                dirty = true;
            }
        }

        /// <summary>
        /// Get and set the photograph for the person 
        /// </summary>
        public string Photograph
        {
            get
            {
                return this.person.Photograph;
            }

            set
            {
                this.person.Photograph = value ?? string.Empty;
                dirty = true;
            }
        }

        /// <summary>
        /// Delete the person from the persistsnace 
        /// </summary>
        public void Delete()
        {
            this.personDal.DeletePerson(this.person);
        }

        /// <summary>
        /// Save the person to the persistance 
        /// </summary>
        public void Save()
        {
            if (dirty || this.person.ID == 0)
            {
                this.personDal.DeletePerson(this.person);
                this.personDal.AddPerson(this.person);
                dirty = false;
            }
        }
    }
}