﻿// <copyright file="ContactController.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model
{    
    using System;
    using System.ComponentModel;

    using StockRightNow.Model.DAL;

    /// <summary>
    /// Controller for looking after observable contacts 
    /// </summary>
    public class ContactController : IEditableObject, IContact
    {
        /// <summary>
        /// the current contact data 
        /// </summary>
        private IContact contact;

        /// <summary>
        /// the dal to use 
        /// </summary>
        private IContactDal dal;

        /// <summary>
        /// the undo information for the contact 
        /// </summary>
        private Contact undo;

        public ContactController(IContactDal dal, IContact contact)
        {
            this.dal = dal;
            this.contact = contact ?? new Contact();
        }

        /// <summary>
        /// Gets or sets the action for this contact. 
        /// </summary>
        public string Action
        {
            get
            {
                return this.contact.Action;
            }

            set
            {
                this.contact.Action = value ?? string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the date of this contact. 
        /// </summary>
        public DateTime ContactDate
        {
            get
            {
                return this.contact.ContactDate;
            }

            set
            {
                this.contact.ContactDate = value;
            }
        }

        /// <summary>
        /// The id of this contact 
        /// </summary>
        public long Id
        {
            get
            {
                return this.contact.Id;
            }

            set
            {
                this.contact.Id = value;
            }
        }

        /// <summary>
        /// Gets or sets the notes for this action. 
        /// </summary>
        public string Notes
        {
            get
            {
                return this.contact.Notes;
            }

            set
            {
                this.contact.Notes = value ?? string.Empty;
            }
        }

        /// <summary>
        /// The id of the person this contact belongs to 
        /// </summary>
        public long PersonId
        {
            get
            {
                return this.contact.PersonId;
            }

            set
            {
                this.contact.PersonId = value;
            }
        }

        /// <summary>
        /// Start editing this objet 
        /// </summary>
        public void BeginEdit()
        {
            this.undo = new Contact();
            this.CopyValues(this.contact, this.undo);
        }

        /// <summary>
        /// Cancel editing this object 
        /// </summary>
        public void CancelEdit()
        {
            this.CopyValues(this.undo, this.contact);
            this.undo = null;
        }

        /// <summary>
        /// finish editing this object 
        /// </summary>
        public void EndEdit()
        {
            if (this.contact.Id > 0)
            {
                this.CancelEdit();
            }
            else
            {
                this.undo = null;
            }
        }

        /// <summary>
        /// Save this contact 
        /// </summary>
        public void Save()
        {
            if (this.contact.Id <= 0 && this.contact.PersonId >= 0)
            {
                dal.AddContact(this.contact);
            }
        }

        /// <summary>
        /// Copy one contacts data to another 
        /// </summary>
        /// <param name="copyFrom"> the contact to copy from </param>
        /// <param name="copyTo">   the contact to copy to </param>
        private void CopyValues(IContact copyFrom, IContact copyTo)
        {
            if ((copyFrom != null) && (copyTo != null))
            {
                copyTo.Action = copyFrom.Action;
                copyTo.ContactDate = copyFrom.ContactDate;
                copyTo.Notes = copyFrom.Notes;
                copyTo.Id = copyFrom.Id;
                copyTo.PersonId = copyFrom.PersonId;
            }
        }
    }
}