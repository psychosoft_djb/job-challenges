﻿// <copyright file="PersonDal.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    /// <summary>
    /// The DAL for the person class. 
    /// </summary>
    public class PersonDal : IPersonDal
    {
        /// <summary>
        /// Adds a person to the database. 
        /// </summary>
        /// <param name="person"> The person to add. </param>
        /// <returns> The ID of the person added or -1. </returns>
        public long AddPerson(IPerson person)
        {
            long id = -1;

            if (person != null)
            {
                using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "dbo.AddPerson";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@Forename", DbType.String, person.Firstname, 50));
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@surname", DbType.String, person.Lastname, 50));
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@dob", DbType.Date, person.DateOfBirth));
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@Picture", DbType.String, person.Photograph));

                        using (var results = command.ExecuteReader())
                        {
                            while (results.Read())
                            {
                                long.TryParse(results["ID"].ToString(), out id);
                                person.ID = id;
                            }
                        }
                    }

                    connection.Close();
                }
            }

            return id;
        }

        /// <summary>
        /// Delete a given person based on ID. 
        /// </summary>
        /// <param name="toDelete"> the person to delete. </param>
        /// <returns> true if the delete worked, false if not. </returns>
        public bool DeletePerson(IPerson toDelete)
        {
            var deleted = false;

            if (toDelete != null)
            {
                using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "dbo.DeletePerson";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@ID", DbType.Int64, toDelete.ID));
                        command.ExecuteNonQuery();
                        deleted = true;
                    }

                    connection.Close();
                }
            }

            return deleted;
        }

        /// <summary>
        /// Gets a person for a given ID 
        /// </summary>
        /// <param name="personId"> The ID to get. </param>
        /// <returns> The loaded person or null. </returns>
        public IPerson GetPerson(long personId)
        {
            Person result = null;

            if (personId > 0)
            {
                using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "dbo.GetPerson";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@ID", DbType.Int64, personId));

                        using (var results = command.ExecuteReader())
                        {
                            while (results.Read())
                            {
                                long id = -1;
                                DateTime dob;

                                long.TryParse(results["ID"].ToString(), out id);

                                // if the id is 0 or less then nothing has been pulled out
                                if (id > 0)
                                {
                                    result = new Person();
                                    result.Firstname = results["Forename"].ToString() ?? string.Empty;
                                    result.Lastname = results["Surname"].ToString() ?? string.Empty;
                                    result.Photograph = results["Image"].ToString() ?? string.Empty;

                                    DateTime.TryParse(results["DateOfBirth"].ToString(), out dob);

                                    result.ID = id;
                                    result.DateOfBirth = dob;
                                }
                            }
                        }
                    }

                    connection.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Get the list of all the person ID's availabe. 
        /// </summary>
        /// <returns> List of all the person IDs. </returns>
        public List<long> GetPersonIds()
        {
            var personIds = new List<long>();

            using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "dbo.GetPersonIDs";
                    command.CommandType = CommandType.StoredProcedure;

                    using (var results = command.ExecuteReader())
                    {
                        long id = -1;

                        while (results.Read())
                        {
                            if (long.TryParse(results["ID"].ToString(), out id))
                            {
                                personIds.Add(id);
                            }
                        }
                    }
                }

                connection.Close();
            }

            return personIds;
        }
    }
}