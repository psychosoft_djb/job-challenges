﻿// <copyright file="ContactDal.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace StockRightNow.Model.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    public class ContactDal : IContactDal
    {
        /// <summary>
        /// Adds a contact to the database. 
        /// </summary>
        /// <param name="contact"> The contact to add. </param>
        /// <returns> The ID of the person added or -1. </returns>
        public long AddContact(IContact contact)
        {
            long id = -1;

            if (contact != null)
            {
                using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "dbo.AddContact";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@PersonID", DbType.Int64, contact.PersonId));
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@action", DbType.String, contact.Action));
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@Notes", DbType.String, contact.Notes));

                        using (var results = command.ExecuteReader())
                        {
                            while (results.Read())
                            {
                                if (long.TryParse(results["ID"].ToString(), out id))
                                {
                                    contact.Id = id;
                                }

                                contact.ContactDate = DateTime.Now;
                            }
                        }
                    }

                    connection.Close();
                }
            }

            return id;
        }

        /// <summary>
        /// Get all the contacts for a person 
        /// </summary>
        /// <param name="person"> the person to get the contacts for </param>
        /// <returns> a list of all the contacts for the person </returns>
        public List<IContact> GetContactHistory(IPerson person)
        {
            var contacts = new List<IContact>();

            if (person != null)
            {
                using (var connection = DatabaseConnection.MakeOpenDatabaseConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "dbo.GetContactHistory";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(DatabaseConnection.MakeNewCommandParameter(command, "@PersonID", DbType.Int64, person.ID));

                        using (var results = command.ExecuteReader())
                        {
                            while (results.Read())
                            {
                                var contact = new Contact() { PersonId = person.ID };
                                DateTime contactDate;
                                long id;

                                contact.Action = results["Action"].ToString() ?? string.Empty;
                                contact.Notes = results["Notes"].ToString() ?? string.Empty;

                                if (DateTime.TryParse(results["Date"].ToString(), out contactDate))
                                {
                                    contact.ContactDate = contactDate;
                                }

                                if (long.TryParse(results["ID"].ToString(), out id))
                                {
                                    contact.Id = id;
                                }

                                contacts.Add(contact);
                            }
                        }
                    }

                    connection.Close();
                }
            }

            return contacts;
        }
    }
}