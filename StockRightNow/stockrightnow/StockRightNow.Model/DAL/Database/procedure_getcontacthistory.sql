USE [StockRightNow]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetContactHistory] (@PersonId int)
AS
BEGIN
	SET NOCOUNT ON;
	Select ID, Date, Action, Notes from dbo.PersonContactHistory where PersonId = @PersonId order by ID
END

GO

