﻿// <copyright file="FeedInterpreterFactory.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Data
{
    using System;
    using System.IO;

    /// <summary>
    /// Feed interpreter factory.
    /// </summary>
    public static class FeedInterpreterFactory
    {
        /// <summary>
        /// File types that can be used.
        /// </summary>
        enum FileTypes
        {
            CSV,
            XML,
            JSON,
            UNKOWN
        }

        /// <summary>
        /// Get sn interpreter for the given filename.
        /// </summary>
        /// <returns>The interpreter or null.</returns>
        /// <param name="filename">The filename to get an interpreter for.</param>
        public static IFeedInterpreter GetInterpreter(string filename)
        {
            IFeedInterpreter interpreter = null;

            switch (GetFiletype(filename))
            {
                case FileTypes.CSV:
                    interpreter = new CsvFeedInterpreter();
                    break;
                case FileTypes.JSON:
                    throw new NotImplementedException();
                    break;
                case FileTypes.XML:
                    throw new NotImplementedException();
                    break;
                default:
                    throw new ArgumentException(String.Format("Do not know how to handle the given filename : {0}", filename ?? "<no filename given>"));
            }

            return interpreter;
        }

        /// <summary>
        /// Gets the filetype for the given filename.
        /// </summary>
        /// <returns>The filetype for the filename.</returns>
        /// <param name="filename">The filename to get the type of.</param>
        private static FileTypes GetFiletype(string filename)
        {
            var fileType = FileTypes.UNKOWN;

            if (!string.IsNullOrWhiteSpace(filename))
            {
                switch (Path.GetExtension(filename).ToUpper())
                {
                    case ".CSV":
                        fileType = FileTypes.CSV;
                        break;
                    case ".XML":
                        fileType = FileTypes.XML;
                        break;
                    case ".JSON":
                        fileType = FileTypes.JSON;
                        break;
                }
            }

            return fileType;
        }
    }
}
