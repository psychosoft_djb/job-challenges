﻿// <copyright file="ItemDiscountValueReport.cs" Author="David Buckley">
//
//  Author:
//    David Buckley david@psychosoft.co.uk
//
//  Copyright (c) 2017, David Buckley
//
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
//       the documentation and/or other materials provided with the distribution.
//     * Neither the name of the [ORGANIZATION] nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace Seriun.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Seriun.Data;

    public class ItemDiscountValueReport : IReport
    {
        /// <summary>
        /// The item code being discounted.
        /// </summary>
        private string itemCode;

        /// <summary>
        /// The discount percentage.
        /// </summary>
        private decimal discount;


        /// <summary>
        /// Gets the result.
        /// </summary>
        /// <returns>The result of a percentage discount being given.</returns>
        /// <param name="orders">The orders to get data from.</param>
        public string GetResult(List<OrderItem> orders)
        {
            var result = string.Empty;

            if ((orders != null) && (!string.IsNullOrEmpty(this.itemCode) && (this.discount >= 0) && (this.discount <= 1)))
            {
                var itemDiscount = (from t in orders
                                    group t by new { t.ItemCode } into grouped
                                    select new
                                    {
                                        grouped.Key.ItemCode,
                                        price = grouped.Max(t => t.Price),
                                        quantity = grouped.Sum(t => t.Quantity)
                                    }
                               ).First(t => string.Equals(t.ItemCode, this.itemCode, StringComparison.InvariantCultureIgnoreCase));

                result = string.Format("{0}% discount on {1} is {2:0.##}", (1 - this.discount) * 100, itemDiscount.ItemCode, ((itemDiscount.price * itemDiscount.quantity) * this.discount));
            }

            return result;
        }

        /// <summary>
        /// Parameterses to use on the report.
        /// First item should be the item code, the second the percentage discount, 20% = 0.2
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        public void ParametersToUse(List<string> parameters)
        {
            if (parameters != null)
            {
                if (parameters.Count <= 2)
                {
                    this.itemCode = parameters[0];
                    decimal amount;
                    decimal.TryParse(parameters[1], out amount);
                    this.discount = 1 - (amount / 100);
                }
            }
        }
    }
}
